//
//  main.m
//  La Morenita
//
//  Created by Juan Revuelta on 6/17/13.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
